import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen'; 
import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { AttendancePage } from '../pages/attendance/attendance';
import { LogoutPage } from '../pages/logout/logout';
import { HttpModule } from '@angular/http';
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation'; 
import { ImageResizer } from '@ionic-native/image-resizer';



@NgModule({ 
  declarations: [  
    MyApp,  
    HomePage,
    ListPage,LoginPage,DashboardPage,
    AttendancePage,
    LogoutPage
  ],
  imports: [
    HttpModule,  
    BrowserModule,
    IonicStorageModule.forRoot({
      name: '__attendanceDB',   
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp], 
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,LoginPage,DashboardPage,
    AttendancePage,
    LogoutPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    Geolocation,
    ImageResizer,  
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
