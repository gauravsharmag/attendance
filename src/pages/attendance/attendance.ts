import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';      
import { Http,Headers } from '@angular/http';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';  
import * as Constants from '../../providers/constants';


import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';

 
@IonicPage()
@Component({ 
  selector: 'page-attendance', 
  templateUrl: 'attendance.html',
})
export class AttendancePage {   
  user_image: string; 
  mapUrl: string;  
  date;   
  public lat;
  public lng;
  public userToken;   
  public userId;
  public mapLat=31.1048;
  public mapLng=77.1734;
  public htmlToAdd;
  public newMapUrl;  
  public employees; 
  public hasEmployees = false; 
  
  constructor(private camera:Camera,public navCtrl: NavController,public navParams: NavParams,private geolocation: Geolocation,public http: Http,public loadingCtrl:LoadingController,public alertCtrl:AlertController,public storage:Storage,private imageResizer: ImageResizer){
  	let date=new Date().toISOString();
  	this.date=date;  
  }  
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad AttendancePage');
    this.storage.get('userToken').then((token)=>{
      if(token){
        this.userToken=token;
        this.getEmployees();
      }
    });
    this.storage.get('initResponse').then((data)=>{
      if(data){
        this.userId=data.success.userid; 
      }
    });   
  } 

  takePicture(){     
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL, 
      mediaType: this.camera.MediaType.PICTURE,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: this.camera.PictureSourceType.CAMERA,
      correctOrientation: true,
      allowEdit: false
    })
      .then(imageUri => {
        this.user_image='data:image/jpeg;base64,' + imageUri;  
        this.imageResizer.resize({
          uri: imageUri,
          quality: 60,
          width: 400,
          height: 400
        }).then(uri => {   
          this.geolocation.getCurrentPosition().then((resp) => {
            this.lat=resp.coords.latitude;
            this.lng=resp.coords.longitude;
            document.getElementById('appendHere').innerHTML='<iframe src = "https://maps.google.com/maps?q='+resp.coords.latitude+','+resp.coords.longitude+'&hl=es;z=8&amp;output=embed"></iframe>';
          }).catch((error) => {
            console.log('Error getting location', error); 
          });
        },(err)=>{
          console.log(err);  
        }) 
      })
      .catch(error => console.warn(error));
  }   

  markAttendance(e){ 
    if(e){
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        let headers=new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('X-Requested-With', 'XMLHttpRequest');
        headers.append('Authorization','Bearer '+this.userToken); 
        let postData={  
          userid:this.userId, 
          image:this.user_image, 
          lat:this.lat,
          lng:this.lng,
          emp_code:e      
        }; 
        this.http.post(Constants.API_ENDPOINT+'/api/mark-attendance',postData,{headers:headers}) 
          .subscribe(data=>{      
            let json=JSON.parse(data['_body']);
            loading.dismiss(); 
            if(json.success.success){
              this.presentAlert('Success','Attendance Submitted'); 
            }else{
              if(json.success.msg){ 
                this.presentAlert('Error',json.success.msg);
              }else{
                this.presentAlert('Error','Request failed,please try again!');
              }
            }
          },error=>{
            console.log(error); 
            loading.dismiss(); 
            this.presentAlert('Error','Request failed,please try again!');
          });
    }else{
      this.presentAlert('Error','Please select employee');
    } 
  }  

  presentAlert(title,subTitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: ['Dismiss']
    });
    alert.present(); 
  }

  askForEmpCode(){
    let alert = this.alertCtrl.create({
      title: 'Enter Employee Code',
      inputs: [
        {
          name: 'emp_code',
          placeholder: 'Employee Code...'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Submit',
          handler: data => {
            this.submitAttendance(data.emp_code);
          }
        }
      ]
    });
    alert.present();
  } 

  submitAttendance(code){
    let loading = this.loadingCtrl.create({content: 'Please wait...'});
    loading.present();
    let headers=new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('X-Requested-With', 'XMLHttpRequest');
    headers.append('Authorization','Bearer '+this.userToken); 
    let postData={  
      userid:this.userId, 
      image:this.user_image, 
      lat:this.lat,
      lng:this.lng,
      emp_code:code      
    }; 
    console.log(postData); 
    this.http.post(Constants.API_ENDPOINT+'/api/mark-attendance',postData,{headers:headers}) 
      .subscribe(data=>{      
        let json=JSON.parse(data['_body']);
        loading.dismiss(); 
        console.log('json.success');
        console.log(json.success.success);
        if(json.success.success){
          this.presentAlert('Success','Attendance Submitted'); 
        }else{
          if(json.success.msg){ 
            this.presentAlert('Error',json.success.msg);
          }else{
            this.presentAlert('Error','Request failed,please try again!');
          }
        }
      },error=>{
        console.log(error);  
        loading.dismiss(); 
        this.presentAlert('Error','Request failed,please try again!');
      });
  }

  getEmployees(){
    let headers=new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('X-Requested-With', 'XMLHttpRequest');
    headers.append('Authorization','Bearer '+this.userToken); 

    this.http.get(Constants.API_ENDPOINT+'/api/get-employees',{headers:headers}) 
      .subscribe(data=>{      
        let jsonEmp=JSON.parse(data['_body']);
        if(jsonEmp.response.emps){
          this.hasEmployees = true;
          this.employees=jsonEmp.response.emps;
        }
      },error=>{
        console.log(error); 
        this.presentAlert('Error','Request failed,please try again!');
      });
  }




}
