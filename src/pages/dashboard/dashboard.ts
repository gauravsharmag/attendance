import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController } from 'ionic-angular';
import { AttendancePage } from '../attendance/attendance';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {  

  constructor(public menu: MenuController,public navCtrl: NavController, public navParams: NavParams) {
    this.menuActive();
  }
  menuActive() {  
    this.menu.enable(true);
  }

  

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

  openCamera(){   
  	// alert('openCamera');
  	this.navCtrl.push(AttendancePage, {}, {animate: true});  
  }

}
