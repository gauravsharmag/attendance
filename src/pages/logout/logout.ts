import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';



@IonicPage()
@Component({ 
  selector: 'page-logout',
  templateUrl: 'logout.html',
})
export class LogoutPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public app:App) {
  	this.app.getRootNav().setRoot(LoginPage);    
  }   

  ionViewDidLoad() {
    console.log('ionViewDidLoad LogoutPage');
  }

}
