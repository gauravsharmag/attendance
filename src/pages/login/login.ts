import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,AlertController,MenuController } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { Storage } from '@ionic/storage';      
import { Http,Headers } from '@angular/http';
import * as Constants from '../../providers/constants'; 


@IonicPage()
@Component({ 
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {  
  
  activeMenu: string;

  constructor(public menu: MenuController,public navCtrl: NavController, public navParams: NavParams,public http: Http,public loadingCtrl:LoadingController,public alertCtrl:AlertController,public storage:Storage){
    this.menuActive(); 
  }
  menuActive() {  
    this.menu.enable(false);
  }

  doLogin(u,p){  
    let loading = this.loadingCtrl.create({content: 'Please wait...'});
    loading.present();
    if(u && p){  
      let headers=new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('X-Requested-With', 'XMLHttpRequest');
      let postData={email:u,password:p};
      this.http.post(Constants.API_ENDPOINT+'/api/login',postData,{headers:headers})   
        .subscribe(data=>{          
          let json=JSON.parse(data['_body']);
          loading.dismiss(); 
          this.storage.set('initResponse', json);
          let jResponse=json.success;
          if(jResponse.success){
            //if(jResponse.role==4){  
              this.storage.set('userToken',jResponse.token); 
              this.navCtrl.push(DashboardPage, {}, {animate: true}); 
            /*}else{
              loading.dismiss();
              this.presentAlert('Error','Sorry,only employees are allowed to login here!');  
            }*/ 
          }else{ 
            loading.dismiss();
            this.presentAlert('Error','Invalid details,please try again!');
          }  
        },error=>{
          loading.dismiss(); 
          this.presentAlert('Error','Invalid details,please try again!');
        });
    }else{
      loading.dismiss();
      this.presentAlert('Error','Please enter username/password');
    }
  }

  presentAlert(title,subTitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: ['Dismiss']
    });
    alert.present(); 
  }  

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
